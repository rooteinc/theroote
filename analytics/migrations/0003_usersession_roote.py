# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-23 20:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0002_remove_location_owner'),
        ('analytics', '0002_auto_20180219_2227'),
    ]

    operations = [
        migrations.AddField(
            model_name='usersession',
            name='roote',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='post.Roote'),
        ),
    ]
