from django.conf.urls import url
from . import views
from django.contrib.auth.views import (
    login, logout, password_reset, password_reset_done, password_reset_confirm,
    password_reset_complete,
)

app_name = 'accounts'
urlpatterns = [
    #loginpage
    url(r'^login/$', login, {'template_name': 'accounts/login.html'}, name='login'),

    #logoutpage
    url(r'^logout/$', logout, {'template_name': 'accounts/logout.html'}, name='logout'),

    #registerpage
    url(r'^register/$', views.register, name='register'),

    # profilepage
    url(r'^profile/$', views.ViewProfile.as_view(), name='view_profile'),

    # friendsprofilepage
    url(r'^profile/(?P<pk>\d+)/$', views.ViewProfile.as_view(), name='view_profile_with_pk'),
    #profileeditpage
    url(r'^profile/edit/$', views.edit_profile, name='edit_profile'),

    #change-passwordpage
    url(r'^profile/password/$', views.change_password, name='change_password'),

    #password_resetPage
    url(r'^reset-password/$', password_reset, {'template_name':
    'accounts/reset_password.html', 'post_reset_redirect': 'accounts:password_reset_done' ,
    'email_template_name':'accounts/reset_password_email.html'}, name='reset_password'),

    #password_reset_done
    url(r'^reset-password/done/$', password_reset_done, {'template_name':'accounts/reset_password_done.html'},
    name='password_reset_done'),

    #sendsemail
    url(r'^reset-password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
    password_reset_confirm, {'template_name':'accounts/reset_password_confirm.html',
    'post_reset_redirect':'accounts:password_reset_complete'}, name='password_reset_confirm'),

    url(r'^reset-password/complete/$', password_reset_complete,
    {'template_name':'accounts/reset_password_complete.html'},name='password_reset_complete'),
    # profilepage
    url(r'^profile/$', views.ViewProfile.as_view(), name='view_profile'),




	]
