from django.shortcuts import render
from django.views.generic import TemplateView
from .models import UserSession

class LatlonView(TemplateView):
    model = UserSession
    template_name = 'analytics/latlon.html'

    context_object_name = 'all_usersession'

    def get(self, request):
        usersessions = UserSession.objects.filter(user = self.request.user)
        args = {'usersessions':usersessions}
        return render(request, self.template_name, args)
