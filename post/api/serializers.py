from rest_framework.serializers import  (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    )
from post.models import Roote,Category,Location


roote_detail_url = HyperlinkedIdentityField(
        view_name = 'post-api:detail',
        lookup_field ='pk',
        )



class  CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ('OUTDOORS','NIGHTLIFE', 'URBANEXPLORE','FOODY')


class RooteListSerializer(ModelSerializer):
    url =roote_detail_url
    owner = SerializerMethodField()
    category =   CategorySerializer(many=True, read_only=True)
    class Meta:
        model  =  Roote
        fields = [
        'category',
        'url',
        'owner',
        'id',
        'roote_name',
        'slug',
        'timestamp',
        ]
    def get_owner(self, obj):
        return str(obj.owner.username)
    def get_category(self, obj):
        return str(obj.category)

class RooteCreateUpdateSerializer(ModelSerializer):
    class Meta:
        model  =  Roote
        fields = [
        'category',
        'roote_name',
        'timestamp',
        ]

class RooteDetailSerializer(ModelSerializer):
    owner = SerializerMethodField()
    url =roote_detail_url
    class Meta:
        model  =  Roote
        fields = [
        'id',
        'roote_name',
        'slug',
        'timestamp',
        'url',
        'owner',
        ]
    def get_owner(self, obj):
        return str(obj.owner.username)


class LocationSerializer(ModelSerializer):
    roote = RooteDetailSerializer()
    class Meta:
        model = Location
        fields = [
			'roote',
            'coordinate',
            'place_name',
        ]
