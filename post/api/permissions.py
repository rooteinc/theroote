from rest_framework.permissions import BasePermission,SAFE_METHODS

class isOwnerOrReadOnly(BasePermission):
    message = 'You must be the owner of this object'
    def has_object_permission(self, request, view, obj):
        if requst.method in SAFE_METHODS:
            return True
        return obj.user == request.user
