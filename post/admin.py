from django.contrib import admin
from .models import Roote, Location, Category 

admin.site.register(Roote)
admin.site.register(Location)
admin.site.register(Category)
