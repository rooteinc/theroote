from django.db import models
from django.core.urlresolvers import reverse
from django.conf import  settings
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save
from .utils import unique_slug_generator
from django.contrib.contenttypes.models import ContentType


__all__=[User,settings]
# Create your models here.

User = settings.AUTH_USER_MODEL

class Category(models.Model):
    OUTDOORS = 'OUTDOORS'
    NIGHTLIFE = 'NIGHTLIFE'
    URBANEXPLORE = 'URBANEXPLORE'
    FOODY = 'FOODY'

    YOUR_CHOICES = (
        (OUTDOORS,'OUTDOORS'),
		(NIGHTLIFE,'NIGHTLIFE'),
		(URBANEXPLORE,'URBANEXPLORE'),
		(FOODY ,'FOODY')
    )
    categories = models.CharField(max_length=100, choices=YOUR_CHOICES, default='null')
    def __str__(self):
        return self.categories


class RooteManager(models.Manager):
    def filter_by_instance(self, instance):
        content_type = ContentType.objects.get_for_model(instance.__class__)
        obj_id = instance
        qs = super(RooteManager, self).filter(content_type=content_type , object_id = obj_id )
        return qs

class Roote(models.Model):
    likes         = models.ManyToManyField(settings.AUTH_USER_MODEL, blank =True, related_name = 'roote_likes')
    roote_name    = models.CharField(max_length=250)
    roote_photo   = models.ImageField(upload_to='roote_image')
    category      = models.ManyToManyField(Category)
    slug          = models.SlugField(null=True, blank=True)
    timestamp     = models.DateTimeField(auto_now_add=True, null=True)
    updated       = models.DateTimeField(auto_now=True, null=True)
    owner         = models.ForeignKey(User)
    objects       = RooteManager()

    def get_absolute_url(self):
    	return reverse('post:detail', kwargs={'pk': self.pk})

    def __str__(self):
    	return self.roote_name

    @property
    def title(self):
    	return self.roote_name

    def get_like_url(self):
        return reverse("post:like-toggle", kwargs={"pk": self.pk})

    def get_api_like_url(self):
        return reverse("post-api:like-api-toggle", kwargs={"pk": self.pk})

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type


def pre_save_roote_receiver(sender, instance, *args, **kwargs):
    print ('saving...')
    if not instance.slug:
        instance.slug = instance.slug = unique_slug_generator(instance)




pre_save.connect(pre_save_roote_receiver, sender=Roote)




class Location(models.Model):
    roote = models.ForeignKey(Roote, on_delete=models.CASCADE, null=True)
    coordinate = models.CharField(max_length=1000)
    place_name = models.CharField(max_length=250)
    city_data       = models.TextField(null=True, blank=True)
    latitude        = models.FloatField(null=True, blank=True)
    longitude       = models.FloatField(null=True, blank=True)
    session_key     = models.CharField(max_length=60, null=True, blank=True)
    ip_address      = models.GenericIPAddressField(null=True, blank=True)
    def __str__(self):
        return  self.place_name


    def get_absolute_url(self):
        return reverse('post:detail', kwargs={'pk': self.pk})
